package com.example.android.layouts;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {


    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Data to display as list
        List<String> forecasts = new ArrayList<>();
        forecasts.add("Today - Sunny - 88/63");
        forecasts.add("Tomorrow - Foggy - 88/63");
        forecasts.add("Weds - Cloudy - 88/63");
        forecasts.add("Thurs - Rainy - 88/63");
        forecasts.add("Fri - Foggy - 88/63");
        forecasts.add("Sat - Sunny - 88/63");
        forecasts.add("Today - Sunny - 88/63");
        forecasts.add("Tomorrow - Foggy - 88/63");
        forecasts.add("Weds - Cloudy - 88/63");
        forecasts.add("Thurs - Rainy - 88/63");
        forecasts.add("Fri - Foggy - 88/63");
        forecasts.add("Sat - Sunny - 88/63");
        forecasts.add("Today - Sunny - 88/63");
        forecasts.add("Tomorrow - Foggy - 88/63");
        forecasts.add("Weds - Cloudy - 88/63");
        forecasts.add("Thurs - Rainy - 88/63");
        forecasts.add("Fri - Foggy - 88/63");
        forecasts.add("Sat - Sunny - 88/63");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getActivity(),
                R.layout.list_item,
                R.id.list_item_value,
                forecasts
        );

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        AdapterView<ListAdapter> adapterView = (AdapterView) view;
        adapterView.setAdapter(adapter);

        return view;
    }

}
